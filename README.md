Orient CABOW

Script used to gather playbacks and detection data, match detections with estimate playback source location and estimate the lander's orientation under water.

To use: 
Run CABOW Config CABOW_MARYLAND_20210922.psfx on base station on jar: PamguardBeta-2.01.05cb.jar with CABOW plugins (UPD_RW_Classifier, and ClipNoiseMeasurement).
Run remote unit with CABOW ASP Configuration pb-xxx-CABOWremote_20210901_000317.xml
Assign a sqlite database to the base station processing and enter full path in this project in python/orient.py as variable "receiveddbLoc"

Create a directory to hold the playback metadata and output in format:
\\path\\to\\some\\parent\\folder\\pbXXX
Where pbXXX is the buoy ID for the unit up for calibration
Assign value for parameter "playbacksDataBase" as "\\path\\to\\some\\parent\\folder\\"

Conduct playbacks approximately 250 meters from lander below thermocline, collect the following data in .csv format:
UTC,lat_start,lon_start,lat_end,lon_end
Save the .csv as \\path\\to\\some\\parent\\folder\\pbXXX\\playbacks_meta.csv

After the playbacks have been completed, run the sciprt python\orient.py with a single parameter as the full buoy ID. Example:

python orient.py pb317

The script will generate output found in "SampleOutput" in this project in a directory timestamped with the time of execution of orient.py

The script will eliminate all detections outside of mean(angleoffset)+/-std(angleoffset)*2.
If the user wishes to bound the calculation for bearing offset further, they can include manual bounds as a second runtime parameter. Example:

python orient.py pb317 "100,180"

This would bound the final output bearing offset to exclude all data points outside of [100,180]
