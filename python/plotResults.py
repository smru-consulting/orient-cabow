from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import datetime

import math
from filteredObjects.detections import detections
from filteredObjects.playbacks import playbacks

def showLatLongPlots(matchedPlaybacksDf,BuoyDetections):
    plt.scatter([BuoyDetections.coords[1]],[BuoyDetections.coords[0]])
    plt.scatter(matchedPlaybacksDf["srcLon"],matchedPlaybacksDf["srcLat"])


def genSNRPlot(matchedPlaybacksDf,matchedPlaybacksDf_Bounded):
    fig, axes = plt.subplots(2, 1)
    sns.scatterplot(x="SNR",y="AngleDifDeg",hue="ClassScore",ax=axes[0],data=matchedPlaybacksDf)
    sns.scatterplot(x="SNR",y="AngleDifDeg",hue="ClassScore",ax=axes[1],data=matchedPlaybacksDf_Bounded)
    axes[0].set_ylabel("Bearing Offset")
    axes[1].set_ylabel("Bounded Bearing Offset")
    fig.set_size_inches((15, 12), forward=False)

def genPolarPlot(matchedPlaybacksDf,matchedPlaybacksDf_Bounded,BuoyDetections):
    fig = plt.figure()
    ax1 = fig.add_subplot(2,2,1,projection='polar')
    for i,row in matchedPlaybacksDf.iterrows():
        thetaT=np.linspace(row["TrueBearing"],row["TrueBearing"],5)
        thetaM=np.linspace(row["Angle_0"],row["Angle_0"],5)
        rT = np.linspace(.9,1,5)
        rM = np.linspace(.9,row["ClassScore"],5)
        ax1.plot(thetaT, rT,c='r')
        ax1.plot(thetaM, rM,c='b')
    ax1.set_theta_zero_location('N')
    ax1.legend(["True Bearing","Measured Bearing"])
    ax1.set_ylim(.9,1)
    ax1.set_theta_direction(-1)
    label_position=ax1.get_rlabel_position()
    ax1.text(np.radians(label_position+10),(ax1.get_rmax()-ax1.get_rmin())/2.+ax1.get_rmin(),'Classifier Score',
        rotation=label_position,ha='center',va='center')

    medianOffset = matchedPlaybacksDf_Bounded["AngleDif"].median()
    
    ax2 = fig.add_subplot(2,2,2,projection='polar')
    for i,row in matchedPlaybacksDf_Bounded.iterrows():
        thetaT=np.linspace(row["TrueBearing"],row["TrueBearing"],5)
        thetaM=np.linspace(row["Angle_0"]+medianOffset,row["Angle_0"]+medianOffset,5)
        rT = np.linspace(.9,1,5)
        rM = np.linspace(.9,row["ClassScore"],5)
        ax2.plot(thetaT, rT,c='r')
        ax2.plot(thetaM, rM,c='b')
    ax2.set_theta_zero_location('N')
    ax2.legend(["True Bearing","Bounded Measured Bearing + Median Offset of Bounded"])
    ax2.set_ylim(.9,1)
    ax2.set_theta_direction(-1)
    label_position=ax1.get_rlabel_position()
    ax2.text(np.radians(label_position+10),(ax2.get_rmax()-ax1.get_rmin())/2.+ax2.get_rmin(),'Classifier Score',
        rotation=label_position,ha='center',va='center')

    

    #Calculate aspect ratio for lat/long display estimation
    lat_all = np.append(matchedPlaybacksDf["srcLat"],BuoyDetections.coords[0])*math.pi/180
    lon_all = np.append(matchedPlaybacksDf["srcLon"],BuoyDetections.coords[1])*math.pi/180
    lat_0 = np.mean(lat_all)
    r = 6371000
    global_x_min = r*(np.min(lon_all))*math.cos(lat_0)
    global_x_max = r*(np.max(lon_all))*math.cos(lat_0)
    global_y_min = r*(np.min(lat_all))
    global_y_max = r*(np.max(lat_all))
    
    global_y_playbacks = r*matchedPlaybacksDf["srcLat"]*math.pi/180-global_y_min
    global_x_playbacks = r*matchedPlaybacksDf["srcLon"]*math.pi/180*math.cos(lat_0)-global_x_min

    global_x_buoy = r*BuoyDetections.coords[1]*math.pi/180*math.cos(lat_0)-global_x_min
    global_y_buoy = r*BuoyDetections.coords[0]*math.pi/180-global_y_min

    ax3 = fig.add_subplot(2,2,3)
    ax3.scatter([global_x_buoy],[global_y_buoy])
    ax3.scatter(global_x_playbacks,global_y_playbacks)

    ax3.set_aspect('equal', adjustable='box')
    ax3.legend(["Buoy X/Y Approx","Playbacks X/Y Approx"])
    ax3.ticklabel_format(useOffset=False)
    ax3.set_xlabel("X Distance (m)")
    ax3.set_ylabel("Y Distance (m)")
    ax3.locator_params(axis='x', nbins=4)

    ax4 = fig.add_subplot(2,2,4)
    sns.scatterplot(x="SNR",y="AngleDifDeg",hue="ClassScore",ax=ax4,data=matchedPlaybacksDf_Bounded)
    ax4.set_ylabel("Bearing Offset (Bounded)")
    fig.set_size_inches((15, 12), forward=False)

def createPlots(matchedPlaybacksDf,BuoyDetections,outputDir,manualBounds=[0,0]):
    raw_median=matchedPlaybacksDf["AngleDifDeg"].median()
    raw_std=matchedPlaybacksDf["AngleDifDeg"].std()
    bounds = [raw_median-raw_std,raw_median+raw_std]
    if manualBounds!=[0,0]:
        bounds = manualBounds
    matchedPlaybacksDf_Bounded =matchedPlaybacksDf[matchedPlaybacksDf["AngleDifDeg"]>bounds[0]]
    matchedPlaybacksDf_Bounded =matchedPlaybacksDf_Bounded[matchedPlaybacksDf_Bounded["AngleDifDeg"]<bounds[1]]

    showLatLongPlots(matchedPlaybacksDf,BuoyDetections)
    
    plt.savefig(outputDir+"PlaybacksGPSFig.png",dpi=500)
    genSNRPlot(matchedPlaybacksDf,matchedPlaybacksDf_Bounded)
    plt.savefig(outputDir+"SNRFig.png",dpi=500)
    genPolarPlot(matchedPlaybacksDf,matchedPlaybacksDf_Bounded,BuoyDetections)
    plt.savefig(outputDir+"FullFigure.png")

    f=open(outputDir+"results.txt", "a+")
    f.write("\n\nWrite Time (Local): "+str(datetime.datetime.now())+"\n")
    f.write("Buoy [Lat,Lon]: ["+str(BuoyDetections.coords[0])+","+str(BuoyDetections.coords[1])+"]\n")
    f.write("Unbounded Count: "+str(matchedPlaybacksDf["AngleDifDeg"].count())+"\n")
    print("Bounded Count: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].count()))
    f.write("Bounded Count: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].count())+"\n")
    print("Bounded Mean: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].mean()))
    f.write("Bounded Mean: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].mean())+"\n")
    print("Bounded Median: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].median()))
    f.write("Bounded Median: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].median())+"\n")
    print("Bounded StdDev: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].std()))
    f.write("Bounded StdDev: "+str(matchedPlaybacksDf_Bounded["AngleDifDeg"].std())+"\n")
    f.write("Bounds: ["+str(bounds[0])+","+str(bounds[1])+"]\n")
    f.write("User notes: \n")
    f.close()
    