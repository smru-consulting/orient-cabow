import sqlite3
import pandas as pd
import numpy as np
import datetime
import math

from filteredObjects import playbacks

#Detections
#Input: 
#   pbIDstr: 
#       Buoy ID in string format
#       Must be in format "pbXXX"
#   dbLoc:
#       full string path to SQLite database
#          Must contain Network_Reciever table with buoy communication from buoy of interest
#          Must contain HydrophoneStreamers table with accurate buoy name and coordinates
#          Must contain Right_Whale table with detections from buoy of interest (and associated bearings)
#          Must contain Clip_Noise_Measurement table with noise measurements of clips for the buoy of interest
#          Must contain NARW_UDP_Classifier table with classifier scores of clips for the buoy of interest
#
#Fields:
#   pbIDstr:
#       string
#       Assigned at construction
#       string representation of buoy id: "pbXXX"
#   pbNumStr:
#       string
#       Assigned at construction
#       String representation of buoy number: "XXX"
#       Network_Reciever table refers to buoy in this manner
#   dbConn:
#       sqlite3.connection
#       Assigned at construction
#       Active database connection to the database located at dbLoc (parameter)
#   channelMapDict
#       dict
#       Assigned at construction
#       Contains each channelBitmap associated with the buoy in the database
#       keys:
#           "clip": referenced for clip-related tables
#           "netRx": reference for the Network_Receiver table
#           "RW": reference for Right Whale table
#   coords:
#       double array
#       assigned at construction
#       contains the GPS coordinates of the buoy [lat,long]
#       
#Will adjust to query based on time constraint so to not query all the data for the buoy

class detections:
    def __init__(self,pbIDstr,dbLoc):
        self.pbIDstr = pbIDstr
        self.pbNumStr = pbIDstr[2:]
        self.dbConn = self.connectDataBase(dbLoc)
        self.channelMapDict,self.coords = self.getBuoyData()
        self.filteredDetectionDf = self.organizeBuoyDetections()

    #Connect to database located at dbLoc
    def connectDataBase(self,dbLoc):
        try:
            conn = sqlite3.connect(dbLoc)
            print("Opened database successfully")
        except:
            print("Could not connect to database")
            quit()
        return conn

    #Called at construction to assign the appropriate channel bitmaps and GPS coordinates to the oobject
    #Pulls these data from database
    def getBuoyData(self):
        try:
            cursor = self.dbConn.execute("""SELECT Network_Receiver.ChannelBitmap,HydrophoneStreamers.Latitude,HydrophoneStreamers.Longitude,Network_Receiver.BuoyId1 
                                        FROM Network_Receiver 
                                        JOIN HydrophoneStreamers on substr(HydrophoneStreamers.Name,6,8) == Network_Receiver.BuoyId1 
                                        WHERE Network_Receiver.BuoyId1 == """+str(self.pbNumStr))
        except:
            print("Could not execute buoy data query.")
            quit()
        netRxMap = 0
        for row in cursor:
            netRxMap = row[0]
            if row[1]!=0 and row[2]!=0:
                lat = row[1]
                lon = row[2]
        RWEdgechannelBitmap = netRxMap
        clipMap = RWEdgechannelBitmap & (RWEdgechannelBitmap>>1) & (RWEdgechannelBitmap>>2)
        print("NetRx channel Bitmap for "+self.pbIDstr+" is "+str(netRxMap)+ ", RWEdgeChannelBitmap:" +str(RWEdgechannelBitmap) +", Latitude: "+str(lat)+", Longitude: "+str(lon))
        return {"netRx":netRxMap,"RW":RWEdgechannelBitmap,"clip":clipMap},[lat,lon]

    #Called at construction to pull table of all detections for the buoy
    #Generates dataframe containing SNR, UTC, Classifier Score, Angle of detection
    def organizeBuoyDetections(self):
        try:
            filterQuery = "SELECT UTC,Angle_0 FROM Right_Whale WHERE ChannelBitmap == "+str(self.channelMapDict["RW"])
        except:
            print("Could not execute right whale data query")
            quit()
        filteredDetectionDf = pd.read_sql_query(filterQuery, self.dbConn)
        filteredDetectionDf["UTC"] = pd.to_datetime(filteredDetectionDf["UTC"])

        filteredDetectionDf = self.attachClips(filteredDetectionDf)
        self.filteredDetectionDf = filteredDetectionDf
        print("Gathered Filtered Detections: ")
        print(filteredDetectionDf)
        return filteredDetectionDf

    #Grabs the clip-associated data for the buoy of interest and merges with the detection data based on time.
    def attachClips(self,filteredDetectionDf):
        clipMeasurementQuery = """SELECT Clip_Noise_Measurement.SNR as SNR,Clip_Noise_Measurement.UTC as UTC,NARW_UDP_Classifier.Score as ClassScore
                                FROM Clip_Noise_Measurement 
                                INNER JOIN NARW_UDP_Classifier on NARW_UDP_Classifier.Clip_UID==Clip_Noise_Measurement.Clip_UID
                                WHERE Clip_Noise_Measurement.ChannelBitmap == """+str(self.channelMapDict["clip"])
        
        try:
            SNRDf = pd.read_sql_query(clipMeasurementQuery, self.dbConn)
        except:
            print("Could not execute clip measurements data query")
            quit()
        SNRDf["UTC"] = pd.to_datetime(SNRDf["UTC"])
        print("Clip Data Loaded:")
        print(SNRDf)
        SNRDf["UTC"] = SNRDf["UTC"] + pd.to_timedelta(1, unit='s')
        combinedDf = filteredDetectionDf.join(SNRDf.set_index('UTC'), on='UTC')
        return combinedDf


    #matchPlaybacks called externally with instance of filteredObjects.playbacks
    #generates dataframe with the corresponding source location and true bearing for each detection
    #includes detection vs true bearing difference
    def matchPlaybacks(self,playbacks):
        playbacksDf = playbacks.getDF()
        self.matchedPlaybacksdf = self.filteredDetectionDf.reset_index()
        self.matchedPlaybacksdf["InSession"] = 0
        self.matchedPlaybacksdf["srcLat"] = 0
        self.matchedPlaybacksdf["srcLon"] = 0
        self.matchedPlaybacksdf["TrueBearing"] = 0
        for i,row in self.matchedPlaybacksdf.iterrows():
            difCol = abs(playbacksDf["UTC"] - row["UTC"])
            minVal = difCol.min()
            minIdx = difCol.idxmin()
            if minVal<datetime.timedelta(seconds=4):
                srcLat = playbacksDf.loc[minIdx,"Lat"]
                srcLon = playbacksDf.loc[minIdx,"Lon"]
                self.matchedPlaybacksdf.loc[i,"srcLat"] = srcLat
                self.matchedPlaybacksdf.loc[i,"srcLon"] = srcLon
                self.matchedPlaybacksdf.loc[i,"TrueBearing"] = self.calcBearing(srcLat,srcLon)
                self.matchedPlaybacksdf.loc[i,"InSession"] = 1
        self.matchedPlaybacksdf = self.matchedPlaybacksdf[self.matchedPlaybacksdf["InSession"]==1]
        self.matchedPlaybacksdf["TrueBearing"] = self.matchedPlaybacksdf.apply(lambda row : self.boundAngle(row["TrueBearing"]),axis=1)
        self.matchedPlaybacksdf["AngleDif"] = self.matchedPlaybacksdf["TrueBearing"] -self.matchedPlaybacksdf["Angle_0"]
        self.matchedPlaybacksdf["AngleDif"] = self.matchedPlaybacksdf.apply(lambda row : self.boundAngle(row["AngleDif"]),axis=1)
        self.matchedPlaybacksdf["AngleDifDeg"] = self.matchedPlaybacksdf["AngleDif"]*180/math.pi
        self.matchedPlaybacksdf = self.matchedPlaybacksdf[self.matchedPlaybacksdf["ClassScore"]>.9]
        print("playbacks matched: ")
        print(self.matchedPlaybacksdf)
        return self.matchedPlaybacksdf

    #Bounds an angle theta (in radians) to [-pi,pi]
    def boundAngle(self,theta):
        while theta<math.pi*-1:
            theta+=math.pi*2
        while theta>math.pi:
            theta-=math.pi*2
        return theta
    
    #calculates bearing from buoy location to [srcLat,srcLon]
    def calcBearing(self,srcLat,srcLon):
        lambda1 = self.coords[1]*math.pi/180
        phi1 = self.coords[0]*math.pi/180
        lambda2 = srcLon*math.pi/180
        phi2 = srcLat*math.pi/180
        dlambda = abs(lambda2-lambda1)%180
        dphi = math.log(math.tan(phi2/2+math.pi/4)/math.tan(phi1/2+math.pi/4))
        return math.atan2(dlambda,dphi)#-math.pi/2
