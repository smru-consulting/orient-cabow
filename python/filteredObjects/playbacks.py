import pandas as pd
import os
import numpy as np
from datetime import datetime

#Playbacks
#   Used for generating interpolated source position given start_lat, end_lat, start_lon, end_lon, and UTC

class playbacks:
    #Constructor: input playback .csv location.
    # CSV must contain columns: start_lat, end_lat, start_lon, end_lon, UTC
    def __init__(self,playbackCSVLoc):
        #Generate Dataframe from CSV
        self.playbacksDF = pd.read_csv(playbackCSVLoc)
        #Hard code playback length will parameterize later
        self.playbackLength = 4
        #Interpolate the motion of playback vessel with .5 second resolution
        self.playbacksDF = self.interpDrift(.5)
        

    #InterpDrift
    #Creates interpolated data for each row of a playback .csv
    #Fills playback dataframe with UTC,lat,lon interpolated based on timeResolutionSeconds
    def interpDrift(self,timeResolutionSeconds):
        #Format datetime column
        self.playbacksDF["UTC"] = pd.to_datetime(self.playbacksDF["UTC"])
        #Generate empty array to be populated with a dataframe for each row in playback csv
        frameList = []
        #For each set of playbacks
        for i,row in self.playbacksDF.iterrows():
            #Generate timeseries with timeResolution for four minutes from start
            tseries = np.linspace(row["UTC"].value,(row["UTC"]+np.timedelta64(self.playbackLength,'m')).value,int(self.playbackLength*60/timeResolutionSeconds))
            trange = [row["UTC"].value,(row["UTC"]+np.timedelta64(self.playbackLength,'m')).value]
            latrange = [row["lat_start"],row["lat_end"]]
            lonrange = [row["lon_start"],row["lon_end"]]
            #Interpolate coordinates with timeseries as domain
            latseries = np.interp(tseries,trange,latrange)
            lonseries = np.interp(tseries,trange,lonrange)
            d = {"UTC":tseries,"Lat":latseries,"Lon":lonseries}
            frameList.append(pd.DataFrame(data=d))
        #Concatenate each playback set to dataframe
        playbackRows = pd.concat(frameList,axis=0)
        playbackRows["UTC"] = pd.to_datetime(playbackRows["UTC"])
        playbackRows = playbackRows.reset_index()
        print("Playbacks Loaded:")
        print(playbackRows)
        return playbackRows

    def getDF(self):
        return self.playbacksDF